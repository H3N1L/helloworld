import java.util.Scanner;

public class HelloWorld {
    public static void main(String[] args) {

        Scanner scn = new Scanner(System.in);
        System.out.println("What is your name?");
        String output = scn.nextLine();

        System.out.println("What year were you born?");
        int birthYear = Integer.parseInt(scn.nextLine());

        System.out.println(welcome(output, birthYear));
        ifStatementCode();

    }

    public static void line(){
        System.out.println("########################");
    }

    public static String welcome(String name, int year){
        int age = 2022 - year;
        line();
        return "Hello " + name + " you are " + age + " years old!";

    }
    public static void ifStatementCode() {
        line();
        int x = 107;

        if (x < 10) {
            System.out.println("x is less than 10");
        }
        else if (x <= 100) {
            System.out.println("x is between 10 and 100");
        }
        else {
            System.out.println("x is greater than 100");
        }

        int y = 17;

        if(y < 10) {
            System.out.println("y is less than 10");
        }
        else {
            System.out.println("y is greater than (or equal to) 10");
        }

        String[] animals = {"cat", "cat", "dog", "tortoise", "cat", "rabbit", "dog", "cat", "dog", "cat"};
        int cat = 0;
        int dog = 0;


        for(int i = 0; i < animals.length; i++){
            if(animals[i] == "cat") {
                cat++;
            }
            else if (animals[i] == "dog") {
                dog++;
            }
        }

        System.out.println("Number of cats: " + cat);
        System.out.println("Number of dogs: " + dog);
    }



    
}
