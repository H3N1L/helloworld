public class Array {
    public static void main(String[] args) {
        int[] numbers = { 1, 2, 3, 4, 4, 6, 7, 8, 9, 10};
        int total = 0;

        for(int i = 0; i < numbers.length; i++) {
            total += numbers[i];
        }
        System.out.println(total);

        System.out.println(numbers[4]);

        numbers[4] = 5;

        for(int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

    }
}
